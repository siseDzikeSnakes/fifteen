import sys

import numpy

from methods.a_star import AStar
from methods.bfs import BFS
from methods.dfs import DFS
from methods.idfs import IDFS
from model.puzzle import Puzzle
from model.siseutils import max_depth

rows = 0
cols = 0
initial_state = []


def read_input(filename):
    global rows, cols
    lines = []
    puzzle = []
    with open(filename) as ins:
        for line in ins:
            lines.append(line)

    rows = int(lines[0].split()[0])
    cols = int(lines[0].split()[1])

    if len(lines) != rows + 1:
        print(-1)
        return False

    for i in range(1, rows + 1):
        puzzle.append(lines[i].split())

    return numpy.array(puzzle).astype(int).tolist()


def main():
    method = sys.argv[1]
    if method not in ('-b', '--bfs', '-d', '--dfs', '-i', '--idfs', '-a', '--a'):
        print(-1)
        return False
    order = sys.argv[2]

    frame = read_input("input.txt")
    if not frame:
        print('-1')
        return
    puzzle = Puzzle(frame, rows, cols)
    if not puzzle:
        print('-1')
        return
    if method in ('-d', '--dfs'):
        dfs = DFS(puzzle, order, max_depth)
        dfs.search_node("", 0)
        result = dfs.solution

        if result is not None:
            print(len(dfs.solution))
            print(dfs.solution.upper())
        else:
            print(-1)

    elif method in ('-b', '--bfs'):
        bfs = BFS(puzzle, order)
        bfs.search_node("")
        result = bfs.solution
        if result is not None:
            print(len(bfs.solution))
            print(bfs.solution.upper())
        else:
            print(-1)

    elif method in ('-i', '--idfs'):
        idfs = IDFS(puzzle, order)
        idfs.search_node()
        result = idfs.solution
        if result is not None:
            print(len(idfs.solution))
            print(idfs.solution.upper())
        else:
            print(-1)
    elif method in ('-a', '--a'):
        strategy = sys.argv[2]
        heuistics = sys.argv[3]
        astar = AStar(puzzle)
        astar.search()
        result = astar.solution
        if result is not None:
            print(len(result))
            print(result)
        else:
            print(-1)


main()
