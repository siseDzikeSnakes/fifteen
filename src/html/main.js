var zero = [3, 3];
var state = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]]
var i;
var length;

var editMode = false;

$(document).ready(function() {
    drawTiles();

    $('#reset').click(function() {
        $('#puzzle').empty();
        drawTiles();
    });

    $('#shuffle').click(function() {
        var steps = $('#steps-input').val();
        i = 0;
        length = steps.length;
        shuffleState(steps);
    });

    $('#solve').click(function() {
        var input = state;
        var method = $('#method').val();
        var order = $('#order').val();
        $.ajax({
            url: "http://localhost:5000/solve/" + input + "/" + method + "/" + order
         }).then(function(data) {
            $('#result').val(data);
         });
    });

    $('#edit').click(function() {
        $(this).toggleClass("active");
        editMode = !editMode;
    });
});

var drawTiles = function() {
    zero = [3, 3];
    state = [[1, 2, 3, 4], [5, 6, 7, 8], [9, 10, 11, 12], [13, 14, 15, 0]];
    var t = 10;
    var l = 10;
    for(var i = 0; i < 4; i++) {
        for(var j = 0; j < 4; j++) {
            var index = i*4 + j + 1;
            if((i + j) != 6)
                $('#puzzle').append('<div id="' + index +
                '" class="tile" style="top:' + t +'px; left:' + l + 'px;">' + index + '</div>');
            l += 110;
        }
        l = 10;
        t += 110;
    }

    $('.tile').click(function() {
        var pos = getPos($(this));
        if(editMode) {
            newValue = prompt();
            if(newValue) {
                if(parseInt(newValue) >= 0 && parseInt(newValue) < 16) {
                    $(this).html(newValue);
                    $(this).attr('id', newValue);
                    state[pos[0]][pos[1]] = parseInt(newValue);
                }
            }
        } else {
            var move = possibleMove(pos);
            if(move) {
                moveTile($(this), move);
            } else {
                $(this).effect( "shake", {times:1}, 200 );
            }
        }
    });
};

var getPos = function(tile) {
    var column = $(tile).css("left").slice(0, -2);
    column = Math.floor(parseInt(column) / 110);
    var row = $(tile).css("top").slice(0, -2);
    row = Math.floor(parseInt(row) / 110);
    return [row, column];
};

var possibleMove = function(pos) {
    var rowDiff = zero[0] - pos[0];
    var colDiff = zero[1] - pos[1];
    if(rowDiff == 0) {
        if(colDiff == 1) {
            return "L";
        } else if(colDiff == -1) {
            return "P";
        }
    } else if(colDiff == 0) {
        if(rowDiff == 1) {
            return "G";
        } else if(rowDiff == -1) {
            return "D";
        }
    }
};

var moveZero = function(step) {
    switch(step.toUpperCase()) {
        case "L":
            if(zero[1] > 0) {
                var id = '#' + state[zero[0]][zero[1] - 1];
                $(id).animate({ left: "+=110" }, 100);
                left();
            }
            break;
        case "P":
            if(zero[1] < 3) {
                var id = '#' + state[zero[0]][zero[1] + 1];
                $(id).animate({ left: "-=110" }, 100);
                right();
            }
            break;
        case "G":
            if(zero[0] > 0) {
                var id = '#' + state[zero[0] - 1][zero[1]];
                $(id).animate({ top: "+=110" }, 100);
                up();
            }
            break;
        case "D":
            if(zero[0] < 3) {
                var id = '#' + state[zero[0] + 1][zero[1]];
                $(id).animate({ top: "-=110" }, 100);
                down();
                break;
            }
    }
}

var moveTile = function(tile, move) {
    switch(move) {
        case "L":
            $(tile).animate({ left: "+=110" }, 100);
            left();
            break;
        case "P":
            $(tile).animate({ left: "-=110" }, 100);
            right();
            break;
        case "G":
            $(tile).animate({ top: "+=110" }, 100);
            up();
            break;
        case "D":
            $(tile).animate({ top: "-=110" }, 100);
            down();
            break;
    }
};

var left = function() {
    state[zero[0]][zero[1]] = state[zero[0]][zero[1] - 1];
    state[zero[0]][zero[1] - 1] = 0;
    zero[1] -= 1;
};

var right = function() {
    state[zero[0]][zero[1]] = state[zero[0]][zero[1] + 1];
    state[zero[0]][zero[1] + 1] = 0;
    zero[1] += 1;
};

var up = function() {
    state[zero[0]][zero[1]] = state[zero[0] - 1][zero[1]];
    state[zero[0] - 1][zero[1]] = 0;
    zero[0] -= 1;
};

var down = function() {
    state[zero[0]][zero[1]] = state[zero[0] + 1][zero[1]];
    state[zero[0] + 1][zero[1]] = 0;
    zero[0] += 1;
};

var shuffleState = function(steps) {
    moveZero(steps[i])
    setTimeout(function() {
        if(i < length - 1) {
            i++;
            shuffleState(steps);
        }
    }, 500)

};

