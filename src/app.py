from flask import Flask, jsonify
from flask.ext.cors import CORS

from methods.a_star import AStar
from methods.bfs import BFS
from methods.dfs import DFS
from methods.idfs import IDFS
from model.puzzle import Puzzle
from model.siseutils import max_depth

app = Flask(__name__)
CORS(app)


@app.route('/solve/<string:frame_in>/<string:method>/<string:order>', methods=['GET'])
def get_tasks(frame_in, method, order):
    frame = []
    for i in range(0, 4):
        frame.append([])
        for j in range(0, 4):
            frame[i].append(int(frame_in.split(',', 16)[(i * 4) + j]))
    puzzle = Puzzle(frame, 4, 4)

    result = ""

    if method == 'd':
        dfs = DFS(puzzle, order, max_depth)
        dfs.search_node("", 0)
        result = dfs.solution
        if result is None:
            result = "-1"

    elif method == 'b':
        bfs = BFS(puzzle, order)
        bfs.search_node("")
        result = bfs.solution
        if result is None:
            result = "-1"

    elif method == 'i':
        idfs = IDFS(puzzle, order)
        idfs.search_node()
        result = idfs.solution
        if result is None:
            result = "-1"

    elif method == 'a':
        astar = AStar(puzzle)
        astar.search()
        result = astar.solution
        if result is None:
            result = "-1"

    return jsonify(result)


if __name__ == '__main__':
    app.run(debug=True)
