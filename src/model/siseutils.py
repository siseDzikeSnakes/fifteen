import random
from copy import deepcopy

max_depth = 7


def generate_steps(n):
    from model import puzzle
    puz = puzzle.Puzzle.fresh()
    steps = ""
    visited = set()
    while n != 0:
        puz_temp = deepcopy(puz)
        visited.add(puz_temp.__hash__())
        step = 'LPGD'[random.randint(0, 3)]
        puz_temp.shuffle_state(step)
        if puz_temp.__hash__() not in visited:
            n -= 1
            puz = deepcopy(puz_temp)
            steps += step
        visited.add(puz_temp.__hash__())
    return steps
