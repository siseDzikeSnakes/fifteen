from copy import deepcopy


def initialize(rows, cols):
    frame = []
    for i in range(0, rows):
        frame.append([])
        for j in range(0, cols):
            frame[i].append((i * cols) + 1 + j)
    frame[rows - 1][cols - 1] = 0
    return frame


class Puzzle:
    initial_state = None

    def __init__(self, puzzle, row_count, col_count):
        self.puzzle = puzzle
        self.col_index = self.get_column_index()
        self.row_index = self.get_row_index()
        self.row_count = row_count
        self.col_count = col_count
        if Puzzle.initial_state is None:
            Puzzle.initial_state = initialize(row_count, col_count)
        pass

    @classmethod
    def fresh(cls):
        return cls(deepcopy(Puzzle.initial_state))

    def __eq__(self, other):
        return self.puzzle == other.puzzle

    def __hash__(self):
        return hash(self.to_string())

    def to_string(self):
        return ('\n'.join([''.join(['{:4}'.format(item) for item in row])
                           for row in self.puzzle]))

    def left(self):
        if self.col_index > 0:
            self.puzzle[self.row_index][self.col_index], self.puzzle[self.row_index][self.col_index - 1] = \
                self.puzzle[self.row_index][self.col_index - 1], self.puzzle[self.row_index][self.col_index]
            self.col_index -= 1
            return True
        else:
            return False

    def right(self):
        if self.col_index < self.col_count - 1:
            self.puzzle[self.row_index][self.col_index], self.puzzle[self.row_index][self.col_index + 1] = \
                self.puzzle[self.row_index][self.col_index + 1], self.puzzle[self.row_index][self.col_index]
            self.col_index += 1
            return True
        else:
            return False

    def up(self):
        if self.row_index > 0:
            self.puzzle[self.row_index][self.col_index], self.puzzle[self.row_index - 1][self.col_index] = \
                self.puzzle[self.row_index - 1][self.col_index], self.puzzle[self.row_index][self.col_index]
            self.row_index -= 1
            return True
        else:
            return False

    def down(self):
        if self.row_index < self.row_count - 1:
            self.puzzle[self.row_index][self.col_index], self.puzzle[self.row_index + 1][self.col_index] = \
                self.puzzle[self.row_index + 1][self.col_index], self.puzzle[self.row_index][self.col_index]
            self.row_index += 1
            return True
        else:
            return False

    def shuffle_state(self, steps):
        for step in steps:
            if step.upper() == "P":
                self.right()
            elif step.upper() == "L":
                self.left()
            elif step.upper() == "G":
                self.up()
            elif step.upper() == "D":
                self.down()
            else:
                return False

    def get_indices(self):
        row_ind = None
        col_ind = None
        for row in self.puzzle:
            try:
                col_ind = row.index(0)
                row_ind = self.puzzle.index(row)
            except ValueError:
                continue
        return [row_ind, col_ind]

    def get_row_index(self):
        return self.get_indices()[0]

    def get_column_index(self):
        return self.get_indices()[1]

    def good(self, node):
        puzzle_temp = Puzzle(deepcopy(self.puzzle), self.row_count, self.col_count)
        puzzle_temp.shuffle_state(node)
        if puzzle_temp.puzzle == Puzzle.initial_state:
            return True
        return False
