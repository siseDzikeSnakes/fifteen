import time

from methods.dfs import DFS
from model import siseutils
from model.puzzle import Puzzle


def find_states(depth, iterations):
    result = set()
    # for i in range(0, iterations):
    while len(result) < iterations:
        steps = siseutils.generate_steps(depth)
        puz = Puzzle.fresh()
        puz.shuffle_state(steps)
        result.add(puz)
        # if i % 10000 == 0:
        #     print(str(i) + " : " + str(len(result)))
    return result

direction = "PDGL"
states = find_states(9, 100)
print("FOUND")
i = 0

times = []
visited = []
recursions = []
depths = []

for p in states:
    i += 1
    if i % 25 == 0:
        print(i)

    dfs = DFS(p, direction, siseutils.max_depth)

    tm = -int(round(time.time() * 1000))
    dfs.search_node('', 0)
    tm += int(round(time.time() * 1000))
    times.append(tm)

    if dfs.solution:
        visited.append(len(dfs.visited))
        recursions.append(dfs.max_recursion)

print("avg_time: " + str(sum(times) / float(len(times))))
print("avg_visited: " + str(sum(visited) / float(len(visited))))
print("avg_recursion: " + str(sum(recursions) / float(len(recursions))))
print()
print(len(times))
