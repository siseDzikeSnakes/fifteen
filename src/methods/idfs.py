from methods.dfs import DFS


class IDFS:
    def __init__(self, puzzle, search_order):
        self.puzzle = puzzle
        self.search_order = search_order
        self.solution = None
        self.depth = 0
        self.visited = 0

    def search_node(self):
        self.solution = "" if self.puzzle.good("") else None

        while self.solution is None:
            self.depth += 1
            dfs = DFS(self.puzzle, self.search_order, self.depth)
            dfs.search_node("", 0)
            self.solution = dfs.solution
            self.visited += len(dfs.visited)
