from copy import deepcopy
from queue import Queue

from model.puzzle import Puzzle
from model.siseutils import max_depth


class BFS:
    def __init__(self, puzzle, search_order):
        self.visited = set()
        self.frontier = Queue(maxsize=0)
        self.solution = None
        self.search_order = search_order
        self.puzzle = puzzle

    def search_node(self, node):
        self.frontier.put(node)
        if self.puzzle.good(""):
            self.solution = ""

        while not self.solution:
            suspect = self.frontier.get()
            puz = Puzzle(deepcopy(self.puzzle.puzzle), self.puzzle.row_count, self.puzzle.col_count)
            puz.shuffle_state(suspect)
            self.visited.add(puz.__hash__())

            if self.puzzle.good(suspect):
                self.solution = suspect
                return

            if suspect not in self.visited:
                for char in self.search_order:
                    self.frontier.put(suspect + char)

            if len(suspect) == max_depth:
                return
