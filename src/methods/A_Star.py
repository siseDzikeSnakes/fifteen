import queue
from copy import deepcopy

from methods.manhattan_distance import MDistance


class AStar:
    def __init__(self, puzzle):
        self.puzzle = puzzle
        self.solution = None
        self.steps = "PDLG"
        self.last_state = 0
        self.visited = set()
        self.distance = MDistance(puzzle)
        self.queue = queue.PriorityQueue(maxsize=0)

    def search(self):
        self.queue.put((self.distance.get(''), ''))
        if self.puzzle.good(""):
            self.solution = ""
        while self.solution is None:
            path = self.queue.get()[1]
            for c in self.steps:
                possible_path = path + c
                puz_temp = deepcopy(self.puzzle)
                puz_temp.shuffle_state(possible_path)
                if puz_temp.__hash__() not in self.visited:
                    self.queue.put((self.distance.get(possible_path), possible_path))
                    self.visited.add(puz_temp.__hash__())

                if self.puzzle.good(possible_path):
                    self.solution = possible_path
