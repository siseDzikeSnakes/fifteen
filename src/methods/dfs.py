from copy import deepcopy

from model.puzzle import Puzzle


class DFS:
    def __init__(self, puzzle, search_order, max_depth):
        self.visited = set()
        self.search_order = search_order
        self.max_depth = max_depth
        self.puzzle = puzzle
        self.solved = True if self.puzzle.good("") else False
        self.solution = "" if self.solved else None
        self.max_recursion = 0

    def search_node(self, node, cur_depth):
        if not self.solved:
            # oznaczenie stanu jako odwiedzony
            puz = Puzzle(deepcopy(self.puzzle.puzzle), self.puzzle.row_count, self.puzzle.col_count)
            puz.shuffle_state(node)
            if cur_depth != self.max_depth:
                self.visited.add(puz.__hash__())

            self.max_recursion = max(cur_depth, self.max_recursion)

            # jeżeli układanka jest rozwiązana następuje przerwanie funkcji
            if self.puzzle.good(node):
                self.solved = True
                self.solution = node
                return

            # algorytm właściwy
            if cur_depth < self.max_depth:
                for char in self.search_order:
                    new_node = node + char
                    puz = Puzzle(deepcopy(self.puzzle.puzzle), self.puzzle.row_count, self.puzzle.col_count)
                    puz.shuffle_state(new_node)
                    if puz.__hash__() not in self.visited:
                        self.search_node(new_node, cur_depth + 1)
