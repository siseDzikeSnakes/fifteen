from copy import deepcopy


def tile_index(tile):
    return [(tile - 1) // 4, (tile - 1) % 4] if tile != 0 else [3, 3]


class MDistance:
    def __init__(self, puzzle):
        self.init_puz = puzzle

    def get(self, path):
        puz_temp = deepcopy(self.init_puz)
        puz_temp.shuffle_state(path)
        frame = puz_temp.puzzle

        distance = len(path)

        for row in range(0, len(frame)):
            for col in range(0, len(frame[row])):
                if frame[row][col] != 0:
                    pos = tile_index(frame[row][col])
                    to_add = abs(pos[0] - row) + abs(pos[1] - col)
                    distance += to_add
        return distance
